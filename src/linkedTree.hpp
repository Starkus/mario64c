#pragma once

#include "linkedList.hpp"

template <typename T>
struct _treenode {
	T value;
	LinkedList<_treenode<T>*> children;
	_treenode<T> *parent;
};

template <typename T>
class LinkedTree {
	LinkedList<_treenode<T>*> roots;

	_treenode<T> *getNodeByValue(LinkedList<_treenode<T>*> nodelist, T item) {
		// Search through all the values first
		for (int i = 0; i < nodelist.size(); ++i) {
			_treenode<T> *node = nodelist.get(i);
			if (node->value == item)
				return node;
		}
		// Then in the children
		for (int i = 0; i < nodelist.size(); ++i) {
			_treenode<T> *node = nodelist.get(i);

			if (node->children.size() == 0)
				continue;

			_treenode<T> *result = getNodeByValue(node->children, item);
			if (result != nullptr)
				return result;
		}
		return nullptr;
	}

	_treenode<T> *getNodeByValue(T item) {
		return getNodeByValue(roots, item);
	}

	void __addToListRecursive(LinkedList<_treenode<T>*> current, LinkedList<T> *target) {
		for (int i = 0; i < current.size(); ++i) {
			_treenode<T> *node = current.get(i);
			target->add(node->value);
			__addToListRecursive(node->children, target);
		}
	}

public:
	LinkedTree() {
	}

	void addRoot(T value) {
		_treenode<T> *newnode = new _treenode<T>;
		newnode->value = value;
		newnode->parent = nullptr;

		roots.add(newnode);
	}

	void addBelow(T value, T parent) {
		_treenode<T> *newnode = new _treenode<T>;
		newnode->value = value;

		_treenode<T> *parentNode = getNodeByValue(roots, parent);
		parentNode->children.add(newnode);
		newnode->parent = parentNode;
	}

	void moveToRoot(T value) {
		_treenode<T> *node = getNodeByItem(value);
		node->parent->children->remove(node);
		roots->add(node);
	}

	void changeParent(T value, T newParent) {
		_treenode<T> *node = getNodeByValue(value);
		_treenode<T> *parentNode = getNodeByValue(newParent);
		if (node->parent)
			node->parent->children.remove(node);
		parentNode->children.add(node);
	}

	LinkedList<T> getRoots() {
		LinkedList<T> list;
		for (int i = 0; i < roots.size(); ++i) {
			list.add(roots.get(i)->value);
		}
		return list;
	}
	
	LinkedList<T> getBelow(T item) {
		LinkedList<T> list;
		_treenode<T> *parent = getNodeByValue(item);
		for (int i = 0; i < parent->children.size(); ++i) {
			list.add(parent->children.get(i)->value);
		}
		return list;
	}

	LinkedList<T> getAll() {
		LinkedList<T> list;
		__addToListRecursive(roots, &list);
		return list;
	}

	LinkedList<T> getBelowRecursive(T item) {
		LinkedList<T> list;
		_treenode<T> *parent = getNodeByValue(item);
		for (int i = 0; i < parent->children.size(); ++i) {
			T child = parent->children.get(i)->value;
			list.add(child);
			LinkedList<T> belowChild = getBelowRecursive(child);
			for (int j = 0; j < belowChild.size(); ++j) {
				list.add(belowChild.get(j));
			}
		}
		return list;
	}
};
