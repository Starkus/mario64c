#include "componentSkeleton.hpp"

#include "linkedList.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include "glcontext.hpp"

LinkedList<GameObject*> CSkeleton::getBones() {
	Scene *scene = obj->getScene();
	return scene->getObjectHierarchy()->getBelowRecursive(obj);
}

void CSkeleton::resetRestPose() {
	Scene* scene = obj->getScene();
	LinkedList<GameObject*> bones = scene->getObjectHierarchy()->getBelowRecursive(obj);

	glm::mat4 skelTransform = obj->transform()->getComputedTransform(); 
	glm::mat4 skelInverse = inverse(skelTransform);

	for (int i=0; i < bones.size(); ++i) {
		GameObject *bone = bones.get(i);
		bone->transform()->computeTransform();
		glm::mat4 poseTransform = bone->transform()->getComputedTransform();
		poseTransform = skelInverse * poseTransform;
		restPose[bones.get(i)] = poseTransform;
	}
}

std::unordered_map<GameObject*, glm::mat4> CSkeleton::getRestPose() {
	return restPose;
}

void CSkeleton::uploadTransformsToGPU(GLuint program, std::unordered_map<GameObject*, int> map) {
	Scene *scene = obj->getScene();
	LinkedList<GameObject*> bones = scene->getObjectHierarchy()->getBelowRecursive(obj);

	mat4 toPoseSpace = inverse(obj->transform()->getComputedTransform());

	for (int i=0; i < bones.size(); ++i) {
		GameObject *bone = bones.get(i);
		bone->transform()->computeTransform();

		mat4 transform = bone->transform()->getComputedTransform();
		transform *= toPoseSpace;
		mat4 bindPose = restPose[bone];
		transform *= inverse(bindPose);

		// Check if key exists first
		if (map.find(bone) == map.end()) {
			//fprintf(stderr, "Bone not in index map: %s\n", bone);
			continue;
		}

		int index = map[bone];

		std::string name = "bones[";
		name += std::to_string(index) + "]";
		GLuint location = glGetUniformLocation(program, name.c_str());
		glUniformMatrix4fv(location, 1, false, &transform[0][0]);
	}
}

void CSkeleton::update() {
	//uploadTransformsToGPU(GLContext::instance()->getCurrentProgram());
}
