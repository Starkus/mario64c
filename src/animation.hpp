#pragma once

#include <glm/glm.hpp>
#include <vector>

#include "gameObject.hpp"

using namespace glm;

struct Keyframe {
	float time;
	mat4 value;
};

class Channel {
public:
	GameObject *target;
	std::vector<Keyframe> keyframes;

	mat4 getInterpolatedValue(float time);
};

class Animation {
public:
	std::vector<Channel> channels;
};