#pragma once

#include <stdlib.h>

#include "gameObject.hpp"
#include "linkedList.hpp"
#include "linkedTree.hpp"

class Scene {
public:
	//LinkedList<GameObject*> gameObjects;
	LinkedTree<GameObject*> objectHierarchy;

	Scene();
	void update();
	
	GameObject* spawn(const char *name, GameObject* parent = nullptr);

	LinkedTree<GameObject*> *getObjectHierarchy();
};