#pragma once

#include "model.hpp"

class SkinnedModel : public Model {
protected:
	std::vector<glm::vec4> boneWeights;
	std::vector<glm::ivec4> boneIndices;
};