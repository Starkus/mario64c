#pragma once

#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <string>

#include "component.hpp"
#include "scene.hpp"
#include "linkedList.hpp"
#include "componentTransform.hpp"

using namespace glm;

class CTransform;

class GameObject {
protected:
	std::string name;

	LinkedList<Component*> components;
	Scene *scene;
	//GameObject *parent = nullptr;

	CTransform *_ctransform;

public:
	GameObject(std::string name, Scene *scene);

	void update();

	LinkedList<Component*> getComponents();

	Scene *getScene();
	//GameObject *getParent();

	void setParent(GameObject *parent);
	GameObject* getParent();

	CTransform *transform();

	template <typename T>
	T* addComponent() {
		T *newComp = new T(this);
		this->components.add(newComp);
		
		return newComp;
	}

	template <typename T>
	T* getComponentOfType() {
		for (int i = 0; i < components.size(); ++i) {
			Component *component = components.get(i);
			if (dynamic_cast<T*>(component) != nullptr) {
				return (T*) component;
			}
		}

		return nullptr;
	}

	void removeComponent(Component*);
};
