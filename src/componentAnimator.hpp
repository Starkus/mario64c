#pragma once

#include "component.hpp"
#include "animation.hpp"
#include <unordered_map>

class CAnimator : public Component {
protected:
	std::unordered_map<string, Animation> animations;
	Animation current;

public:
	CAnimator(GameObject *obj) : Component(obj) {}
	void start() override;
	void update() override;

	void addAnimation(std::string, Animation);
};