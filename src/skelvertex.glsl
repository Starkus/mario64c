#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal_modelspace;
layout(location = 3) in vec4 boneIndices;
layout(location = 4) in vec4 boneWeights;

out vec2 UV;
out vec3 Normal_cameraspace;
out vec3 LightDirection_cameraspace;
out vec4 matDiffuse;
out vec4 matSpeculat;
out vec4 matAmbient;
out vec4 matEmmision;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform mat4 MVP;

uniform mat4 bones[64];

uniform vec4 diffuse;
uniform vec4 specular;
uniform vec4 ambient;
uniform vec4 emmision;

void main(){
	UV = vertexUV;
	matDiffuse = diffuse;
	matSpeculat = specular;
	matAmbient = ambient;
	matEmmision = emmision;

	vec4 vertex = vec4(vertexPosition_modelspace, 1);
	vec4 normal = vec4(vertexNormal_modelspace, 0);

	vec4 newVertex, newNormal;
	int index;

	index = int(boneIndices.x);
	newVertex = (bones[index] * vertex) * boneWeights.x;
	newNormal = (bones[index] * normal) * boneWeights.x;
	
	index = int(boneIndices.y);
	if (index != -1) {
		newVertex += (bones[index] * vertex) * boneWeights.y;
		newNormal += (bones[index] * normal) * boneWeights.y;
	}

	newVertex = vec4(newVertex.xyz, 1);

	gl_Position = MVP * newVertex;

	vec3 vertexPosition_cameraspace = (V * M * vertex).xyz;
	vec3 EyeDirection_cameraspace = vec3(0, 0, 0) - vertexPosition_cameraspace;

	vec3 LightPosition_worldspace = vec3(2, 1, 2);

	vec3 LightPosition_cameraspace = (V * vec4(LightPosition_worldspace, 1)).xyz;
	LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;

	Normal_cameraspace = (V * M * newNormal).xyz;
}

