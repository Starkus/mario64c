#pragma once

#include <stdlib.h>
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <unordered_map>

#include "material.hpp"

using namespace glm;

class Model {
private:
	// Model data
	std::vector<vec3> vertices;
	std::vector<vec2> uvs;
	std::vector<vec3> normals;
	std::vector<unsigned short> indices;

	std::vector<Material> materials;
	// What indices belong to what material index
	std::vector<unsigned short> offsets;

	// Map that remembers where each vertex came from
	std::unordered_map<int, int> indexedToRaw;

	// GL Buffers
	GLuint vertexbuffer;
	GLuint uvbuffer;
	GLuint normalbuffer;
	GLuint indexbuffer;

	void bindBuffers();
	void sendMaterial(Material);
	void drawBuffers();

public:
	Model();
	~Model();
	void draw();

	std::vector<vec3>* getVertices();
	std::vector<vec3>* getNormals();
	std::vector<vec2>* getUVs();
	std::vector<unsigned short>* getIndices();
	std::vector<unsigned short>* getOffsets();
	std::vector<Material>* getMaterials();

	void setIndexedToRawMap(std::unordered_map<int, int>);
	std::unordered_map<int, int> getIndexedToRawMap();

	void makeVBOs();
};