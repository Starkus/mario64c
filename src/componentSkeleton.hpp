#pragma once

#include "component.hpp"

#include <unordered_map>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include"gameObject.hpp"

class CSkeleton : public Component {
protected:
	std::unordered_map<GameObject*, glm::mat4> restPose;
public:
	LinkedList<GameObject*> getBones();
	CSkeleton(GameObject *owner) : Component(owner) {}
	void resetRestPose();
	std::unordered_map<GameObject*, glm::mat4> getRestPose();
	void uploadTransformsToGPU(GLuint, std::unordered_map<GameObject*, int>);

	void update() override;
};
