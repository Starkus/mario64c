// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

#include <time.h>

#include <glm/gtc/matrix_transform.hpp>

#include "common/shader.hpp"
#include "common/texture.hpp"
#include "common/controls.hpp"
#include "common/objloader.hpp"
#include "common/vboindexer.hpp"

#include "glcontext.hpp"
#include "time.hpp"
#include "model.hpp"
#include "scene.hpp"
#include "componentMeshRenderer.hpp"
#include "componentTransform.hpp"
#include "daeLoader.hpp"

// Include GLM
#include <glm/glm.hpp>
using namespace glm;


Scene scene;


const int WIDTH = 1024;
const int HEIGHT = 768;


int main(int argc, char *argv[])
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(WIDTH, HEIGHT, "Mario 64 C++", nullptr, nullptr);
	if( window == nullptr ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible."
			   	"Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Create a VAO
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// A texture
	GLuint image = loadDDS("../media/mario.dds");
	if (!image) {
		fprintf(stderr, "Failed to load sample texture");
		return -1;
	}

	// Load some shaders
	GLuint programID = LoadShaders("vertex.glsl", "fragment.glsl");
	GLuint MMatrixID = glGetUniformLocation(programID, "M");
	GLuint VMatrixID = glGetUniformLocation(programID, "V");
	GLuint PMatrixID = glGetUniformLocation(programID, "P");
	GLuint MVPMatrixID = glGetUniformLocation(programID, "MVP");

	GLContext::instance()->setCurrentProgram(programID);


	DaeLoader daeLoader("../media/skeltest.dae");
	daeLoader.load(&scene);


	// Create the game timer
	Time::begin();

	// GAME LOOP
	do {
		Time::beginTick();

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		glEnable(GL_CULL_FACE);
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(programID);

		// Debug FPS cam
		computeMatricesFromInputs();
		mat4 proj = getProjectionMatrix();
		mat4 view = getViewMatrix();

		// Send matrices
		GLuint viewMatrixId = GLContext::instance()->getProgramUniform("V");
		GLuint projMatrixId = GLContext::instance()->getProgramUniform("P");
		glUniformMatrix4fv(viewMatrixId, 1, GL_FALSE, &view[0][0]);
		glUniformMatrix4fv(projMatrixId, 1, GL_FALSE, &proj[0][0]);

		
		scene.update();


		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0);

	// Cleanup VBO and shader
	glDeleteProgram(programID);
	glDeleteTextures(1, &image);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

