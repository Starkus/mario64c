#include "scene.hpp"
#include "componentMeshRenderer.hpp"

Scene::Scene() {
}

void Scene::update() {
	// Update all the objects

	LinkedList<GameObject*> objects = objectHierarchy.getAll();

	//for (int i = 0; i < gameObjects.size(); ++i) {
	for (int i = 0; i < objects.size(); ++i) {
		GameObject *obj = objects.get(i);

		obj->update();
	}

	// Compute transforms
	for (int i = 0; i < objects.size(); ++i) {
		GameObject *obj = objects.get(i);
		CTransform *transform = obj->getComponentOfType<CTransform>();

		transform->computeTransform();
	}
}

GameObject* Scene::spawn(const char *name, GameObject *parent) {
	printf("Spawned an object\n");
	GameObject *newObj = new GameObject(name, this);

	//gameObjects.add(newObj);
	if (parent == nullptr)
		objectHierarchy.addRoot(newObj);
	else
		objectHierarchy.addBelow(newObj, parent);

	return newObj;
}

LinkedTree<GameObject*>* Scene::getObjectHierarchy() {
	return &objectHierarchy;
}