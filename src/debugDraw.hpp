#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "glcontext.hpp"

using namespace glm;

namespace ddraw {
	void setColor(vec4 color);
	void drawLine(vec3 v0, vec3 v1);
}