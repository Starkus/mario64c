#pragma once

#include "model.hpp"
#include "gameObject.hpp"
#include <string>
#include <vector>
#include <unordered_map>

class Controller {
protected:
	Model *mesh;
	std::vector<glm::vec4> weights;
	std::vector<glm::vec4> indices;
	std::unordered_map<std::string, int> boneIndices;

public:
	std::string meshId;

	void setMesh(Model *mesh);
	Model* getMesh();

	void setWeights(std::vector<glm::vec4>);
	std::vector<glm::vec4> getWeights();

	void setIndices(std::vector<glm::vec4>);
	std::vector<glm::vec4> getIndices();

	void setBoneIndices(std::unordered_map<std::string, int>);
	std::unordered_map<std::string, int> getBoneIndices();
};
