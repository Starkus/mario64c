#include "component.hpp"

Component::Component(GameObject *owner) {
	obj = owner;
}

GameObject* Component::getObject() {
	return obj;
}
