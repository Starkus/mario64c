#include "componentTransform.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/matrix_decompose.hpp>

#include "glcontext.hpp"
#include "common/controls.hpp"


CTransform::CTransform(GameObject *owner) : Component(owner) {
	position = vec3(0);
	rotation = quat(1, 0, 0, 0);
	scale = vec3(1.0f);
}

vec3 CTransform::getPosition() {
	return vec3(position);
}

quat CTransform::getRotation() {
	return quat(rotation);
}

vec3 CTransform::getScale() {
	return vec3(scale);
}

mat4 CTransform::getTransformMatrix() {
	mat4 rot = mat4_cast(rotation);

	mat4 localTransform = mat4(1.0f);
	localTransform = glm::translate(localTransform, position);
	localTransform *= rot;
	localTransform = glm::scale(localTransform, scale);

	return localTransform;
}

vec3 CTransform::getWorldPosition() {
	return vec3(worldPosition);
}

quat CTransform::getWorldRotation() {
	return quat(worldRotation);
}

vec3 CTransform::getWorldScale() {
	return vec3(worldScale);
}

mat4 CTransform::getComputedTransform() {
	return mat4(computedTransform);
}

CTransform* CTransform::getParent() {
	return parent;
}


void CTransform::setPosition(vec3 newPosition) {
	position = newPosition;

	setDirty(true);
}

void CTransform::setRotation(quat newRotation) {
	rotation = newRotation;

	setDirty(true);
}

void CTransform::setScale(vec3 newScale) {
	scale = newScale;

	setDirty(true);
}

void CTransform::setTransform(mat4 newTransform) {
	vec3 pos, scale, skew;
	vec4 whatever;
	quat orient;

	decompose(newTransform, scale, orient, pos,
		skew, whatever);

	position = pos;
	this->scale = scale;
	rotation = orient;

	setDirty(true);
}


void CTransform::bindModelMatrix() {
	CTransform *transform = obj->getComponentOfType<CTransform>();
	transform->computeTransform();
	// Model matrix
	mat4 modelMat = transform->getComputedTransform();
	// Calculate MVP
	mat4 mvp = getProjectionMatrix() * getViewMatrix() * modelMat;

	// Send new matrix to the program (the shaders)
	GLuint modelMatrixId = GLContext::instance()->getProgramUniform("M");
	GLuint mvpMatrixId = GLContext::instance()->getProgramUniform("MVP");
	glUniformMatrix4fv(modelMatrixId, 1, GL_FALSE, &modelMat[0][0]);
	glUniformMatrix4fv(mvpMatrixId, 1, GL_FALSE, &mvp[0][0]);

	// These two since we might have changed the program
	mat4 proj = getProjectionMatrix();
	mat4 view = getViewMatrix();
	GLuint viewMatrixId = GLContext::instance()->getProgramUniform("V");
	GLuint projMatrixId = GLContext::instance()->getProgramUniform("P");
	glUniformMatrix4fv(viewMatrixId, 1, GL_FALSE, &view[0][0]);
	glUniformMatrix4fv(projMatrixId, 1, GL_FALSE, &proj[0][0]);
}


bool CTransform::isDirty() {
	if (_dirty)
		return true;
	if (parent != nullptr && parent->isDirty())
		return true;
	return false;
}

void CTransform::setDirty(bool dirty) {
	_dirty = dirty;
}


void CTransform::computeTransform() {
	if (isDirty() == false)
		return;

	mat4 localTransform = getTransformMatrix();

	if (parent == nullptr) {
		worldPosition = position;
		worldRotation = rotation;
		worldScale = scale;
		computedTransform = localTransform;
	}
	else {
		parent->computeTransform();

		computedTransform = parent->getComputedTransform() * glm::scale(mat4(1), scale) * localTransform * glm::scale(mat4(1), 1.0f / scale);
		computedTransform = parent->getComputedTransform() * localTransform;

		worldPosition = vec3(parent->getComputedTransform() * vec4(position / parent->getWorldScale(), 1));
		worldScale = vec3(parent->getComputedTransform() * vec4(scale, 0));
		worldRotation = rotation * parent->getWorldRotation();
	}

	setDirty(false);
}

void CTransform::setParent(CTransform *parent) {
	parent->computeTransform();

	mat4 inv = inverse(parent->getComputedTransform());

	// World position to new local position
	position = vec3(inv * vec4(position, 1));

	// World rotation to new local rotation
	quat rq = quat(inv);
	rq = glm::normalize(rq);
	rotation = rq * rotation;

	// Scale
	scale /= parent->getWorldScale();

	this->parent = parent;
	setDirty(true);

	// Update object hierarchy
	obj->getScene()->getObjectHierarchy()->changeParent(obj, parent->obj);
}
