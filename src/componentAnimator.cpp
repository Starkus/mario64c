#include "componentAnimator.hpp"

#include <vector>

#include "time.hpp"

void CAnimator::start() {

}

void CAnimator::update() {
	float time = Time::time();

	for (std::vector<Channel>::iterator c = animation.channels.begin(); c != animation.channels.end(); ++c) {
		Channel channel = *c;

		mat4 mat = channel.getInterpolatedValue(mod(time, 1.625f));

		channel.target->transform()->setTransform(mat);
	}
}