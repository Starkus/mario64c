#include "debugDraw.hpp"

// This will identify our vertex buffer
GLuint __debugVBO = -1;

void ddraw::setColor(vec4 color) {
	GLuint diffuseId = GLContext::instance()->getProgramUniform("diffuse");
	GLuint specularId = GLContext::instance()->getProgramUniform("specular");
	GLuint ambientId = GLContext::instance()->getProgramUniform("ambient");
	GLuint emmisionId = GLContext::instance()->getProgramUniform("emmision");

	glUniform4f(emmisionId, color.r, color.g, color.b, color.a);
	glUniform4f(diffuseId, 0, 0, 0, 0);
	glUniform4f(specularId, 0, 0, 0, 0);
	glUniform4f(ambientId, 0, 0, 0, 0);
}

void ddraw::drawLine(vec3 v0, vec3 v1) {

	glDisable(GL_DEPTH_TEST);

	const GLfloat g_vertex_buffer_data[] = {
		v0.x, v0.y, v0.z,
		v1.x, v1.y, v1.z,
	};

	if (__debugVBO == (GLuint)(-1)) {
		// Generate 1 buffer, put the resulting identifier in vertexbuffer
		glGenBuffers(1, &__debugVBO);
	}
	
	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, __debugVBO);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	// 1st attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, __debugVBO);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);
	// Draw the triangle !
	glDrawArrays(GL_LINES, 0, 2); // Starting from vertex 0; 3 vertices total -> 1 triangle
	glDisableVertexAttribArray(0);

	glEnable(GL_DEPTH_TEST);
}