#pragma once

#include "componentMeshRenderer.hpp"
#include <unordered_map>
#include "common/shader.hpp"

class CSkinnedMeshRenderer : public CMeshRenderer {
protected:
	std::vector<glm::vec4> weights;
	std::vector<glm::vec4> indices;
	std::unordered_map<GameObject*, int> boneIndexMap;

	GLuint weightsBuffer, indicesBuffer;

	GLuint program;
public:
	CSkinnedMeshRenderer(GameObject *owner) : CMeshRenderer(owner) {}

	void makeBufferObjects();

	void setWeights(std::vector<glm::vec4>);
	void setIndices(std::vector<glm::vec4>);
	void setBoneIndices(std::unordered_map<GameObject*, int>);

	void update() override;
};
