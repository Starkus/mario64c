#pragma once

#include <stdlib.h>
#include <fstream>
#include <vector>
#include <memory>
#include <string>
#include <unordered_map>
#include <glm/glm.hpp>
#include "common/vboindexer.hpp"
#include "rapidxml.hpp"
#include "stringUtils.hpp"
#include "material.hpp"
#include "model.hpp"
#include "controller.hpp"
#include "scene.hpp"
#include "animation.hpp"
#include "componentAnimator.hpp"

using namespace glm;
using namespace rapidxml;
using namespace std;


//int loadModelFromDae(const char *filename, float scale, vector<vec3> *vertices, vector<vec3> *normals,
	//vector<vec2> *uvs, vector<unsigned short> *indices, vector<Material> *materials,
	//vector<int> *offsets);

//int loadSkelFromDae(const char *filename, Scene *scene);

class DaeLoader {
protected:
	const char *filename;

	unordered_map<string, Model> meshes;
	unordered_map<string, Controller> controllers;
	unordered_map<string, Material> materials;
	unordered_map<string, GameObject*> objects;
	unordered_map<string, Animation> animations;

	int __readFile(const char *filename, unique_ptr<string> &contentstr, xml_document<> *out);

	xml_node<>* __getMesh(xml_document<> *doc);
	void __loadMaterials(xml_node<> *docRoot);
	std::unordered_map<int, int> __generateNeededBundles(xml_node<>*, vector<vec3>*, vector<vec3>*, vector<vec2>*,
		vector<vec3>*, vector<vec3>*, vector<vec2>*, vector<Material>*, vector<unsigned short>*);

	void __loadModel(xml_node<> *mesh, Model *model, float scale);
	void __loadLibraryGeometries(xml_node<> *docRoot);
	void __loadLibraryControllers(xml_node<> *docRoot);
	void __loadLibraryAnimations(xml_node<> *docRoot);

	shared_ptr<Model> __instanceModel(string id, xml_node<> *bindMatNode);

	void __loadAllChildren(xml_node<> *node, Scene *scene, GameObject *parent = nullptr, bool isParentJoint = false);
	void __loadVisualScene(xml_node<> *docRoot, Scene *scene);

public:
	DaeLoader(const char *filename) : filename(filename) {}

	int load(Scene *scene);
};