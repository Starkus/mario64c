#include "animation.hpp"

#include <glm/gtx/matrix_interpolation.hpp>

mat4 Channel::getInterpolatedValue(float time) {
	Keyframe a, b;
	for (int i = 0; i < keyframes.size(); i++) {
		Keyframe k = keyframes[i];

		if (k.time == time)
			return k.value;
		else if (k.time < time)
			a = k;
		else {
			b = k;
			break;
		}
	}

	float delta = (time - a.time) / (b.time - a.time);

	mat4 interpolated = interpolate(a.value, b.value, delta);

	return interpolated;
}