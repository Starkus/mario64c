#pragma once

template <typename T>
struct _node {
	T value;
	_node<T> *next;
	_node<T> *previous;
};

template <typename T>
class LinkedList {
	_node<T>* root;

	_node<T>* getLastNode() {

		_node<T>* last = root;
		if (!root)
			return nullptr;

		while (true) {
			if (!last->next)
				return last;
			last = last->next;
		}
	}

	_node<T>* getNodeByIndex(int i) {
		_node<T> *current = root;
		for (int j = 0; j < i; ++j) {
			current = current->next;
			if (!current)
				break;
		}
		return current;
	}

	_node<T>* getNodeByValue(T item) {
		_node<T> *current = root;
		for (int j = 0; j < size(); ++j) {
			if (current->value == item)
				return current;

			current = current->next;
		}
	}

public:
	LinkedList() {
		root = nullptr;
	}
	
	int size() {
		int s = 0;
		_node<T> *current = root;
		while (true) {
			if (current == nullptr)
				return s;
			current = current->next;
			s++;
		}
		return s;
	}

	void add(T value) {
		_node<T> *newnode = new _node<T>;
		newnode->value = value;
		newnode->next = nullptr;

		if (!root)
			root = newnode;
		else {
			_node<T> *last = getLastNode();
			last->next = newnode;
			newnode->previous = last;
		}
	}

	T get(int index) {
		if (index < 0) {
			index = size() + index;
		}
		_node<T> *node = getNodeByIndex(index);
		return node->value;
	}

	void remove(int index) {
		if (index == 0) {
			_node<T> *second = root->next;
			delete root;
			root = second;
		}
		else {
			_node<T> *prev = getNodeByIndex(index - 1);
			_node<T> *next = prev->next->next;
			delete prev->next;
			prev->next = next;
		}
	}

	void remove(T item) {
		_node<T> *node = getNodeByValue(item);
		if (node == root) {
			_node<T> *second = root->next;
			delete node;
			root = second;
		}
		else {
			node->previous->next = node->next;
			node->next->previous = node->previous;
			delete node;
		}
	}
};
