#include "controller.hpp"

Model* Controller::getMesh() {
	return mesh;
}

void Controller::setMesh(Model *mesh) {
	this->mesh = mesh;
}

void Controller::setWeights(std::vector<glm::vec4> weights) {
	this->weights = weights;
}

std::vector<glm::vec4> Controller::getWeights() {
	return weights;
}

void Controller::setIndices(std::vector<glm::vec4> indices) {
	this->indices = indices;
}

std::vector<glm::vec4> Controller::getIndices() {
	return indices;
}

void Controller::setBoneIndices(std::unordered_map<std::string, int> map) {
	boneIndices = map;
}

std::unordered_map<std::string, int> Controller::getBoneIndices() {
	return boneIndices;
}
