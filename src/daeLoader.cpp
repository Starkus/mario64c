#include "daeLoader.hpp"

#include <memory>
#include <glm/gtx/matrix_decompose.hpp>
#include "stringUtils.hpp"
#include "componentMeshRenderer.hpp"
#include "componentSkeleton.hpp"
#include "componentSkinnedMeshRenderer.hpp"
#include <algorithm>

using namespace glm;
using namespace rapidxml;

void __vec4FromString(const char *str, vec4 *vec) {
	vector<string> words = splitString(str, ' ');
	vec->x = stof(words[0]);
	vec->y = stof(words[1]);
	vec->z = stof(words[2]);
	vec->w = stof(words[3]);
}

void __mat4FromString(const char *str,mat4 *mat) {
	vector<string> words = splitString(str, ' ');
	for (int i=0; i < 16; i++)
		(*mat)[i%4][i/4] = stof(words[i]); 
}


int DaeLoader::__readFile(const char *filename, unique_ptr<string> &contentstr, xml_document<> *out) {
	ifstream in(filename, ios::in | ios::binary);
	if (!in)
	{
		printf("Impossible to open COLLADA file '%s'\n", filename);
		return 1;
	}

	in.seekg(0, ios::end);
	contentstr->resize(in.tellg());
	in.seekg(0, ios::beg);
	in.read(&(*contentstr)[0], contentstr->size());
	in.close();

	// Create the xml file
	out->parse<0>((char*)contentstr->c_str());

	xml_node<> *root = out->first_node("COLLADA");

	return 0;
}

xml_node<>* DaeLoader::__getMesh(xml_document<> *doc) {
	// Get the big nodes
	xml_node<> *root = doc->first_node("COLLADA");
	xml_node<> *geometry = root->first_node("library_geometries")->
		first_node("geometry");
	// Just first mesh for now
	return geometry->first_node("mesh");
}

void DaeLoader::__loadMaterials(xml_node<> *docRoot) {
	// Loads materials into the unordered map
	xml_node<> *effects = docRoot->first_node("library_effects");

	for (xml_node<> *n = effects->first_node("effect"); n;
		n = n->next_sibling("effect")) {

		xml_node<> *t = n->first_node()->first_node("technique")->first_node();
		Material mat;

		const char *s;
		string name = n->first_attribute("id")->value();
		//string name = splitString(s, '-')[0];
		name = name.substr(0, name.find_last_of('-'));

		xml_node<> *col;

		// Values
		col = t->first_node("emission")->first_node("color");
		if (col) {
			s = col->value();
			__vec4FromString(s, &mat.emission);
		}

		col = t->first_node("ambient")->first_node("color");
		if (col) {
			s = col->value();
			__vec4FromString(s, &mat.ambient);
		}

		col = t->first_node("diffuse")->first_node("color");
		if (col) {
			s = col->value();
			__vec4FromString(s, &mat.diffuse);
		}

		col = t->first_node("specular")->first_node("color");
		if (col) {
			s = col->value();
			__vec4FromString(s, &mat.specular);
		}

		materials[name] = mat;
	}
}

void __loadUpRawData(xml_node<> *mesh, vector<vec3> *verts, vector<vec3> *norms, vector<vec2> *uvs, 
	float scale) {
	// Iterate through the source nodes, which have vertices, normals and uvs
	for (xml_node<> *n = mesh->first_node("source"); n;
		n = n->next_sibling("source")) {
		std::string source = n->first_attribute("id")->value();

		// Get the float array
		const char* s = n->first_node("float_array")->value();

		// Check where we're at outside of the loop below
		bool atVerts = source.find("positions") != string::npos;
		bool atNorms = source.find("normals") != string::npos;
		bool atUVs = source.find("texcoord") != string::npos;

		// Array is in a string so split it into "words"
		vector<string> words = splitString(s, ' ');

		float x, y;

		int vecIndex = 0;
		for (vector<string>::iterator w = words.begin(); w != words.end(); ++w) {
			float cur = stof(*w);

			switch (vecIndex) {
			case 0:
				x = cur;
				break;

			case 1:
				y = cur;

				if (atUVs) {
					uvs->push_back(vec2(x, y));
					vecIndex = 0;
					continue;
				}
				break;

			case 2:
				if (atVerts)
					verts->push_back(vec3(x, y, cur) * scale);
				else if (atNorms)
					norms->push_back(vec3(x, y, cur));

				break;
			}

			// Increment vector index and loop back to 0 at 3
			vecIndex = (vecIndex + 1) % 3;
		}
	}
}

std::unordered_map<int, int> DaeLoader::__generateNeededBundles(xml_node<> *mesh, vector<vec3> *verts_in, vector<vec3> *norms_in, vector<vec2> *uvs_in,
	vector<vec3> *verts_out, vector<vec3> *norms_out, vector<vec2> *uvs_out,
	vector<Material> *mats_out, vector<unsigned short> *offsets) {

	// This map stores where every vertex comes from
	std::unordered_map<int, int> map;

	// Generate bundles
	// Iterate through all triangles
	for (xml_node<> *n = mesh->first_node("triangles"); n;
		n = n->next_sibling("triangles")) {

		const char* s = n->first_node("p")->value();

		vector<string> words = splitString(s, ' ');
		vec3 vert;
		vec2 uv;
		vec3 norm;

		// Read meta info about how to read the indices
		int vertOffset = -1, normOffset = -1, uvOffset = -1;
		for (xml_node<> *input = n->first_node("input"); input;
				input = input->next_sibling("input")) {
			int offset = stoi(input->first_attribute("offset")->value());
			string semantic = input->first_attribute("semantic")->value();

			if (semantic == "VERTEX")
				vertOffset = offset;
			else if (semantic == "NORMAL")
				normOffset = offset;
			else if (semantic == "TEXCOORD")
				uvOffset = offset;
		}
		int maxOffset = std::max(vertOffset, std::max(normOffset, uvOffset));

		// Now iterate through the indices
		int i = 0;
		for (vector<string>::iterator w = words.begin(); w != words.end(); ++w) {
			int vecIndex = stoi(*w);

			if (i == vertOffset) {
				verts_out->push_back((*verts_in)[vecIndex]);
				// Remember where the new vertex came for for later
				map[verts_out->size() - 1] = vecIndex;
			}
			if (i == normOffset)
				norms_out->push_back((*norms_in)[vecIndex]);
			if (i == uvOffset)
				uvs_out->push_back((*uvs_in)[vecIndex]);

			i = (i + 1) % (maxOffset + 1);
		}
		offsets->push_back(verts_out->size());
	}
	// Last offset points to the end, we don't need it.
	if (offsets->size() > 0)
		offsets->pop_back();

	return map;
}

void DaeLoader::__loadModel(xml_node<> *mesh, Model *model, float scale) {
	vector<vec3> raw_vertices;
	vector<vec2> raw_uvs;
	vector<vec3> raw_normals;
	vector<unsigned short> raw_offsets;

	vector<vec3> ordered_vertices;
	vector<vec2> ordered_uvs;
	vector<vec3> ordered_normals;
	vector<unsigned short> ordered_offsets;

	__loadUpRawData(mesh, &raw_vertices, &raw_normals, &raw_uvs, scale);

	std::unordered_map<int, int> bundledToRawMap, bundledToIndexedMap;

	bundledToRawMap =
	__generateNeededBundles(mesh, &raw_vertices, &raw_normals, &raw_uvs,
		&ordered_vertices, &ordered_normals, &ordered_uvs,
		model->getMaterials(), &ordered_offsets);

	// Now index in OpenGLs way and put it on the model object
	bundledToIndexedMap = 
	indexVBO(ordered_vertices, ordered_uvs, ordered_normals, ordered_offsets,
		*(model->getIndices()), *(model->getVertices()), *(model->getUVs()), *(model->getNormals()), *(model->getOffsets()));

	// Create the indexed to raw map
	std::unordered_map<int, int> indexedToRawMap;
	for (int i = 0; i < ordered_vertices.size(); ++i) {
		indexedToRawMap[bundledToIndexedMap[i]] = bundledToRawMap[i];
	}

	model->setIndexedToRawMap(indexedToRawMap);
}

void __recursiveSkelLoad(xml_node<> *node, Scene *scene, GameObject *parent = nullptr) {
	for (xml_node<> *n = node->first_node("node"); n;
				n = n->next_sibling("node")) {
		GameObject *newob = scene->spawn(n->first_attribute("name")->value());

		mat4 transform;

		__mat4FromString(n->first_node("matrix")->value(), &transform);

		newob->transform()->setTransform(transform);

		if (parent) {
			newob->setParent(parent);
		}

		__recursiveSkelLoad(n, scene, newob);
	}
}

void DaeLoader::__loadLibraryGeometries(xml_node<> *docRoot) {
	xml_node<> *library_geometries = docRoot->first_node("library_geometries");

	for (xml_node<> *geom = library_geometries->first_node("geometry"); geom;
		geom = geom->next_sibling("geometry")) {

		string id = geom->first_attribute("id")->value();
		Model model;

		xml_node<> *mesh = geom->first_node("mesh");

		__loadModel(mesh, &model, 1.0f);

		meshes[id] = model;
	}
}

void DaeLoader::__loadLibraryControllers(xml_node<> *docRoot) {
	xml_node<> *library_controllers = docRoot->first_node("library_controllers");

	for (xml_node<> *cont = library_controllers->first_node("controller"); cont;
		cont = cont->next_sibling("controller")) {

		string id = cont->first_attribute("id")->value();
		Controller controller;

		xml_node<> *skin = cont->first_node("skin");

		controller.meshId = skin->first_attribute("source")->value();
		controller.meshId = controller.meshId.substr(1);

		unordered_map<std::string, int> boneIndexMap;
		vector<string> weightsArr;

		vector<glm::vec4> weights;
		vector<glm::vec4> indices;

		for (xml_node<> *src = skin->first_node("source"); src; src = src->next_sibling("source")) {
			string srcid = src->first_attribute("id")->value();

			if (srcid.find("-joints") != -1) {
				const char *bonesStr = src->first_node("Name_array")->value();
				vector<string> boneNames = splitString(bonesStr, ' ');

				for (int i = 0; i < boneNames.size(); ++i) {
					boneIndexMap[boneNames[i]] = i;
				}

				// send boneIndexMap somewhere maybe??
				controller.setBoneIndices(boneIndexMap);
			}
			else if (srcid.find("-weights") != -1) {
				const char *weightsStr = src->first_node("float_array")->value();
				weightsArr = splitString(weightsStr, ' ');
			}
		}

		xml_node<> *src = skin->first_node("vertex_weights");

		const char *weightCountStr = src->first_node("vcount")->value();
		vector<string> weightCountArr = splitString(weightCountStr, ' ');

		const char *weightIndicesStr = src->first_node("v")->value();
		vector<string> weightIndicesArr = splitString(weightIndicesStr, ' ');

		// Indices and weights are given to the raw vertices, but we have them
		// indexed so we need to map them later
		std::vector<vec4> rawIndices, rawWeights;

		int arrayIndex = 0;
		for (int vertIndex = 0; vertIndex < weightCountArr.size(); ++vertIndex) {
			int count = stoi(weightCountArr[vertIndex]);
			count = std::min(4, count);
			vec4 indicesVec, weightsVec;
			for (int i = 0; i < count; ++i) {
				for (int semantic = 0; semantic < 2; ++semantic) {
					int w = stoi(weightIndicesArr[arrayIndex]);
					if (semantic == 0) {
						indicesVec[i] = w;
					}
					else if (semantic == 1) {
						float weight = stof(weightsArr[w]);
						weightsVec[i] = weight;
					}
					++arrayIndex;
				}
			}
			for (int i = count; i < 4; ++i) {
				indicesVec[i] = -1;
				weightsVec[i] = -1;
			}
			rawIndices.push_back(indicesVec);
			rawWeights.push_back(weightsVec);
		}

		Model model = meshes[controller.meshId];
		std::unordered_map<int, int> vertexMap = model.getIndexedToRawMap();
		for (int i = 0; i < model.getVertices()->size(); ++i) {
			indices.push_back(rawIndices[vertexMap[i]]);
			weights.push_back(rawWeights[vertexMap[i]]);
		}

		controller.setIndices(indices);
		controller.setWeights(weights);

		controllers[id] = controller;
	}
}

void DaeLoader::__loadLibraryAnimations(xml_node<> *docRoot) {
	xml_node<> *library_animations = docRoot->first_node("library_animations");

	CAnimator *animator = objects.begin()->second->addComponent<CAnimator>();

	for (xml_node<> *anim = library_animations->first_node("animation"); anim;
		anim = anim->next_sibling("animation")) {

		string id = anim->first_attribute("id")->value();
		int i1 = id.find_first_of('-');
		int i2 = id.find_last_of('-');
		string animName = id.substr(0, i1);
		string obName = id.substr(i1 + 1, i2 - i1 - 1);

		Channel channel;
		GameObject *target = objects[obName];
		channel.target = target;

		// Fix the stupid no reason why rotation of root bones
		bool rootBone = false;
		GameObject *parent = target->getParent();
		if (parent->getComponentOfType<CSkeleton>() != nullptr) {
			rootBone = true;
		}

		for (xml_node<> *source = anim->first_node("source"); source;
			source = source->next_sibling("source")) {
			
			string type = source->first_attribute("id")->value();
			type = type.substr(id.size() + 1);

			if (type == "input") {
				// Get the float array
				const char* s = source->first_node("float_array")->value();

				// Array is in a string so split it into "words"
				vector<string> words = splitString(s, ' ');
				for (vector<string>::iterator w = words.begin(); w != words.end(); ++w) {
					Keyframe keyframe;
					keyframe.time = stof(*w);
					channel.keyframes.push_back(keyframe);
				}
			}
			else if (type == "matrix-output") {
				// Get the float array
				const char* s = source->first_node("float_array")->value();

				// Array is in a string so split it into "words"
				vector<string> words = splitString(s, ' ');

				for (int i = 0; i < channel.keyframes.size(); ++i) {
					mat4 matrix;
					for (int y = 0; y < 4; ++y)
						for (int x = 0; x < 4; ++x)
							matrix[y][x] = stof(words[i * 16 + y * 4 + x]);

					// Stupid collada exporter rotates root bones for no reason
					if (rootBone) {
						mat4 mat;
						mat[0] = vec4(1, 0, 0, 0);
						mat[1] = vec4(0, 0, 1, 0);
						mat[2] = vec4(0,-1, 0, 0);
						mat[3] = vec4(0, 0, 0, 1);

						matrix = mat * matrix;
					} else {
						vec3 pos, scale, skew;
						vec4 whatever;
						quat orient;

						decompose(matrix, scale, orient, pos,
							skew, whatever);

						orient = quat(orient.w, orient.x, orient.z, -orient.y);

						matrix = glm::translate(glm::scale(mat4_cast(orient), scale), pos);
					}

					matrix = target->transform()->getTransformMatrix() * matrix;

					channel.keyframes[i].value = matrix;
				}
			}
		}

		// If anim don't exist yet
		if (animations.find(animName) == animations.end()) {
			Animation animation;
			animation.channels.push_back(channel);
			animations[animName] = animation;
		}
		else {
			animations[animName].channels.push_back(channel);
		}

		animator->animation = animations[animName];
	}
}

shared_ptr<Model> DaeLoader::__instanceModel(string id, xml_node<> *bindMatNode) {
	shared_ptr<Model> mesh(new Model);
	*mesh = meshes[id];
	mesh->makeVBOs();

	//unordered_map<string, string> symbolMap;
	xml_node<> *technique = bindMatNode->first_node("technique_common");
	for (xml_node<> *matNode = technique->first_node("instance_material"); matNode;
		matNode = matNode->next_sibling("instance_material")) {

		string matName = matNode->first_attribute("target")->value();
		if (matName[0] == '#')
			matName = matName.substr(1);
		matName = matName.substr(0, matName.find_last_of('-')); // Remove -material suffix
		Material mat = materials[matName];
		mesh->getMaterials()->push_back(mat);
	}

	return mesh;
}

void DaeLoader::__loadAllChildren(xml_node<> *node, Scene *scene, GameObject *parent, bool isParentJoint) {
	for (xml_node<> *n = node->first_node("node"); n;
		n = n->next_sibling("node")) {

		const char *obName = n->first_attribute("name")->value();
		GameObject *newob = scene->spawn(obName);
		objects[obName] = newob;

		bool amIJoint = string(n->first_attribute("type")->value()) == "JOINT";

		if (parent) {
			newob->setParent(parent);

			if (amIJoint && !isParentJoint) {
				parent->addComponent<CSkeleton>();
			}
		}

		mat4 transform;
		vec3 pos, scale, skew;
		vec4 whatever;
		quat orient;

		__mat4FromString(n->first_node("matrix")->value(), &transform);

		if (amIJoint && !isParentJoint) {
			// Stupid collada exporter rotates root bones for no reason
			mat4 mat;
			mat[0] = vec4(1, 0, 0, 0);
			mat[1] = vec4(0, 0,-1, 0);
			mat[2] = vec4(0, 1, 0, 0);
			mat[3] = vec4(0, 0, 0, 1);

			transform = mat * transform;
		}

		decompose(transform, scale, orient, pos,
			skew, whatever);

		if (amIJoint)
			// Fix dumb coordinate system
			pos = vec3(pos.x, -pos.z, pos.y);

		newob->transform()->setPosition(pos);
		newob->transform()->setScale(scale);
		newob->transform()->setRotation(orient);

		// Check for instance geometry node
		xml_node<> *geomNode = n->first_node("instance_geometry");
		if (geomNode) {
			string meshId = geomNode->first_attribute("url")->value();
			meshId = meshId.substr(1);

			CMeshRenderer *renderer = newob->addComponent<CMeshRenderer>();//renderer->setModel(meshes[meshId]);

			xml_node<> *bindMatNode = geomNode->first_node("bind_material");
			shared_ptr<Model> mesh = __instanceModel(meshId, bindMatNode);

			renderer->setModel(mesh);
		}

		// Check for controller node
		xml_node<> *contNode = n->first_node("instance_controller");
		if (contNode) {
			string skeletonId = contNode->first_node("skeleton")->value();
			skeletonId = skeletonId.substr(1);
			GameObject *skeletonRootOb = objects[skeletonId];

			string contId = contNode->first_attribute("url")->value();
			contId = contId.substr(1); // Remove #
			Controller controller = controllers[contId];

			xml_node<> *bindMatNode = contNode->first_node("bind_material");
			shared_ptr<Model> mesh = __instanceModel(controller.meshId, bindMatNode);

			CSkinnedMeshRenderer *renderer = newob->addComponent<CSkinnedMeshRenderer>();
			renderer->setModel(mesh);
			renderer->setWeights(controller.getWeights());
			renderer->setIndices(controller.getIndices());

			std::unordered_map<std::string, int> boneNameMap = controller.getBoneIndices();
			std::unordered_map<GameObject*, int> boneObMap;
			for (auto element : boneNameMap) {
				boneObMap[objects[element.first]] = element.second;
			}
			renderer->setBoneIndices(boneObMap);

			renderer->makeBufferObjects();
		}

		__loadAllChildren(n, scene, newob, amIJoint);
	}
}

void DaeLoader::__loadVisualScene(xml_node<> *docRoot, Scene *scene) {
	// Load all visual scene nodes into game scene

	xml_node<> *libVisualScenes = docRoot->first_node("library_visual_scenes");
	xml_node<> *visualScene = libVisualScenes->first_node("visual_scene");

	__loadAllChildren(visualScene, scene);

	// Rest poses
	LinkedList<GameObject*> allObs = scene->getObjectHierarchy()->getAll();
	for (int i=0; i < allObs.size(); ++i) {
		GameObject* ob = allObs.get(i);
		CSkeleton* skeleton = ob->getComponentOfType<CSkeleton>();
		if (skeleton) {
			skeleton->resetRestPose();
		}
	}
}


int DaeLoader::load(Scene *scene) {
	xml_document<> doc;
	unique_ptr<string> contentstr(new string);

	int errorcode = __readFile(filename, contentstr, &doc);
	if (errorcode != 0)
		return errorcode;

	xml_node<> *docRoot = doc.first_node("COLLADA");

	__loadMaterials(docRoot);
	__loadLibraryGeometries(docRoot);
	__loadLibraryControllers(docRoot);

	__loadVisualScene(docRoot, scene);

	__loadLibraryAnimations(docRoot);

	return 0;
}
