#pragma once

#include <memory>

#include "component.hpp"
#include "model.hpp"

class CMeshRenderer : public Component {
protected:
	std::shared_ptr<Model> model;

	void makeModelMatrix(vec3 position, quat rotation, vec3 scale);

public:
	CMeshRenderer(GameObject*);

	void setModel(std::shared_ptr<Model>);

	//void start() override;
	void update() override;
};
