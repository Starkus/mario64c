#pragma once

#include <vector>
#include <string>

std::vector<std::string> splitString(const char* input, char splitter);