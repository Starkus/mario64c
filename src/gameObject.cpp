#include "gameObject.hpp"

#include "debugDraw.hpp"

GameObject::GameObject(std::string name, Scene *scene) : scene(scene) {
	this->name = name;
	this->scene = scene;

	_ctransform = new CTransform(this);
	components.add(_ctransform);
}

void GameObject::update() {
	for (int i = 0; i < components.size(); ++i) {
		//printf("Updating Component");
		Component *comp = components.get(i);
		comp->update();
	}

	_ctransform->bindModelMatrix();
	
	float size = 0.5f;
	ddraw::setColor(vec4(1.0f, 0, 0, 1.0f));
	ddraw::drawLine(vec3(0), vec3(size, 0, 0));

	ddraw::setColor(vec4(0, 1.0f, 0, 1.0f));
	ddraw::drawLine(vec3(0), vec3(0, size, 0));

	ddraw::setColor(vec4(0, 0, 1.0f, 1.0f));
	ddraw::drawLine(vec3(0), vec3(0, 0, size));
}

LinkedList<Component*> GameObject::getComponents() {
	return components;
}

Scene* GameObject::getScene() {
	return scene;
}

/*GameObject* GameObject::getParent() {
	return parent;
}*/

void GameObject::setParent(GameObject *parent) {
	CTransform *transform = getComponentOfType<CTransform>();
	CTransform *parentTransform = parent->getComponentOfType<CTransform>();
	transform->setParent(parentTransform);
}

GameObject* GameObject::getParent() {
	CTransform *parentT = _ctransform->getParent();
	if (parentT == nullptr)
		return nullptr;
	return parentT->getObject();
}

CTransform* GameObject::transform() {
	return _ctransform;
}

void GameObject::removeComponent(Component *component) {
	components.remove(component);
}