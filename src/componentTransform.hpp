#pragma once

#include "component.hpp"
#include "scene.hpp"

class Scene;

class CTransform : public Component {
protected:
	glm::vec3 position;
	glm::quat rotation;
	glm::vec3 scale;

	glm::vec3 worldPosition;
	glm::quat worldRotation;
	glm::vec3 worldScale;

	Scene *scene;
	CTransform *parent = nullptr;

	glm::mat4 computedTransform;
	bool _dirty = true;

public:
	CTransform(GameObject*);

	glm::vec3 getPosition();
	glm::quat getRotation();
	glm::vec3 getScale();
	glm::mat4 getTransformMatrix();

	glm::vec3 getWorldPosition();
	glm::quat getWorldRotation();
	glm::vec3 getWorldScale();
	glm::mat4 getComputedTransform();

	CTransform* getParent();

	void setPosition(glm::vec3);
	void setRotation(glm::quat);
	void setScale(glm::vec3);
	void setTransform(glm::mat4);
	void computeTransform();

	void bindModelMatrix();

	bool isDirty();
	void setDirty(bool);

	void setParent(CTransform *parent);
};
