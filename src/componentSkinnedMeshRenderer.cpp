#include "componentSkinnedMeshRenderer.hpp"

#include "componentSkeleton.hpp"
#include "glcontext.hpp"

void CSkinnedMeshRenderer::setIndices(std::vector<glm::vec4> indices) {
	this->indices = indices;
}

void CSkinnedMeshRenderer::setWeights(std::vector<glm::vec4> weights) {
	this->weights = weights;
}

void CSkinnedMeshRenderer::setBoneIndices(std::unordered_map<GameObject*, int> map) {
	boneIndexMap = map;
}


void CSkinnedMeshRenderer::makeBufferObjects() {
	glGenBuffers(1, &indicesBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, indicesBuffer);
	glBufferData(GL_ARRAY_BUFFER, indices.size() * sizeof(vec4), &indices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &weightsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, weightsBuffer);
	glBufferData(GL_ARRAY_BUFFER, weights.size() * sizeof(vec4), &weights[0], GL_STATIC_DRAW);

	program = LoadShaders("skelvertex.glsl", "fragment.glsl");
}


void CSkinnedMeshRenderer::update() {
	GLuint previousProgram = GLContext::instance()->getCurrentProgram();
	GLContext::instance()->setCurrentProgram(program);

	CSkeleton *skel = obj->transform()->getParent()->getObject()->getComponentOfType<CSkeleton>();
	skel->uploadTransformsToGPU(GLContext::instance()->getCurrentProgram(), boneIndexMap);

	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, indicesBuffer);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(4);
	glBindBuffer(GL_ARRAY_BUFFER, weightsBuffer);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);
	
	CMeshRenderer::update();

	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	GLContext::instance()->setCurrentProgram(previousProgram);
}
